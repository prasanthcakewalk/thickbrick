# Welcome to ThickBrick!

ThickBrick is a Python 3 implementation of certain data selection and categorization algorithms originally conceived in the context of data analysis in high energy physics.

The algorithms are intended to train event selectors and categorizers that maximize the sensitivity of physics analyses to the presence of a signal being searched for, or to the value of a parameter being measured.

Project website: <https://prasanthcakewalk.gitlab.io/thickbrick/>  

### References and citation guide

If you use the algorithms implemented in ThickBrick in your work, please consider citing the original papers that introduced them.

* Konstantin T. Matchev, Prasanth Shyamsundar, _"Optimal event selection and categorization in high energy physics, Part 1: Signal discovery"_, [arXiv:1911.12299 [physics.data-an]](https://arxiv.org/abs/1911.12299).
* Parts 2 and 3 to follow.

This list does not include the now-mainstream algorithms and ideas from mathematics, statistics, machine learning, etc, used in the package. The package documentation mentions the methods used where appropriate.

### Copyright

Copyright &copy; 2019-2020 Konstantin T. Matchev and Prasanth Shyamsundar  
<details>
<summary>ThickBrick is licensed under the MIT License (click to expand).</summary>
<br>
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
</details>
