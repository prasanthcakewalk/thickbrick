__all__ = ['Autocategorizer', 'Evaluator', 'FunctionalCategorizer', 'statdists']

from ._thickbrick import Autocategorizer, Evaluator, FunctionalCategorizer
from . import _statisticaldistances as statdists
